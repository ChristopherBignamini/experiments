"""
This module contains the transfer function which is responsible for applying the linear gain
to the output of the nengo brain
"""
@nrp.MapSpikeSink("out", nrp.brain.out, nrp.raw_signal)
#@nrp.Neuron2Robot(Topic('/salamander_positions', gazebo_msgs.msg.JointPositions))
@nrp.MapRobotPublisher('salamander_positions',Topic('/salamander_positions', gazebo_msgs.msg.JointPositions))
@nrp.Neuron2Robot()
def linear_gain(t,salamander_positions, out):
    """
    The transfer function which calculates the target position for salamander joints.

    :param t: the current simulation time
    :param out: connection to the 11 dimensional output signal of the Nengo brain
    :return: a gazebo_msgs/JointPositions message setting the target position for the salamander joints.
    """
    msg = gazebo_msgs.msg.JointPositions()
    msg.names = ''
    msg.positions = 2*out.value
    salamander_positions.send_message(msg)